import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './pages/main/main.component';
import { CarouselComponent } from './common/carousel/carousel.component';
import { VolgogradComponent } from './pages/volgograd/volgograd.component';
import { OmskComponent } from './pages/omsk/omsk.component';
import { ModalComponent } from './common/modal/modal.component';
import { PetersburgComponent } from './pages/petersburg/petersburg.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { FooterComponent } from './footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CardComponent } from './common/galleryHtmlComponent/card/card.component';
import { MainCardComponent } from './common/main-card/main-card.component';
import { GalleryPageService } from "./services/gallery-page.service";

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        MainComponent,
        CarouselComponent,
        VolgogradComponent,
        OmskComponent,
        ModalComponent,
        PetersburgComponent,
        PortfolioComponent,
        FooterComponent,
        CardComponent,
        MainCardComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
    ],
    providers: [GalleryPageService],
    bootstrap: [AppComponent]
})
export class AppModule {
}

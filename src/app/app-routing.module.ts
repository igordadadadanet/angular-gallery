import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from "./pages/main/main.component";
import { VolgogradComponent } from "./pages/volgograd/volgograd.component";
import { OmskComponent } from "./pages/omsk/omsk.component";
import { PetersburgComponent } from "./pages/petersburg/petersburg.component";
import { PortfolioComponent } from "./pages/portfolio/portfolio.component";
import { BrowserModule } from "@angular/platform-browser";

const appRoutes: Routes = [
    {path: '', component: MainComponent},
    {path: 'volgograd', component: VolgogradComponent},
    {path: 'omsk', component: OmskComponent},
    {path: `petersburg`, component: PetersburgComponent},
    {path: `portfolio`, component: PortfolioComponent},
    {path: `**`, component: MainComponent},
]

@NgModule({
    imports: [BrowserModule, RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

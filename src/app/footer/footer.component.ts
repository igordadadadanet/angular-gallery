import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-footer',
    template: `
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3 mt-4 mb-4">
						<p class="footer__title">
							Контакты: </p>
						<ul class="list-unstyled list list-group">
							<li>
								<a class="text-decoration-none list__link" href="mailto:igordadadadanet@yandex.ru"><i class="link__icon message-icon"></i>Написать</a>
							</li>
							<li>
								<a class="text-decoration-none list__link" href="tel:+79195439516"><i class="link__icon call-icon"></i>Позвонить</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    `,
    styles: []
})
export class FooterComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
    }

}

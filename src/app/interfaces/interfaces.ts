export interface GalleryItem {
    src: string,
    title: string,
    description: string,
}

export interface MainItem extends GalleryItem {
    link: string
}

export interface GalleryComponent {
    data: GalleryItem[],
    title: string
}

export class LoadingComponent {
    public loading = true;

    onLoad() {
        this.loading = false;
    }
}
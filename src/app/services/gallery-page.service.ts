import { Injectable } from '@angular/core';
import { GalleryItem, MainItem } from "../interfaces/interfaces";


@Injectable()
export class GalleryPageService {
  public omskGallery: GalleryItem[] = [
    {title: `omsk 1`, src: `assets/img/omsk/image1.jpg`, description: `р. Иртыш`},
    {title: `omsk 2`, src: `assets/img/omsk/image2.jpg`, description: `Нефтеперерабатывающий завод`},
    {title: `omsk 3`, src: `assets/img/omsk/image3.jpg`, description: `Парк`},
    {title: `omsk 4`, src: `assets/img/omsk/image4.jpg`, description: `р. Иртыш`},
    {title: `omsk 5`, src: `assets/img/omsk/image5.jpg`, description: `Парк Победы`},
    {title: `omsk 6`, src: `assets/img/omsk/image6.jpg`, description: `Омский Городовой`}
  ];

  public volgogradGallery: GalleryItem[] = [
    {title: `volgograd 1`, src: `assets/img/volgograd/image1.jpg`, description: `р. Волга`},
    {title: `volgograd 2`, src: `assets/img/volgograd/image2.jpg`, description: `маяк в Красноармейском р-не`},
    {title: `volgograd 3`, src: `assets/img/volgograd/image3.jpg`, description: `Мамаев Курган`},
    {title: `volgograd 4`, src: `assets/img/volgograd/image4.jpg`, description: `судоходный канал`},
    {title: `volgograd 5`, src: `assets/img/volgograd/image5.jpg`, description: `Камышин`}
  ];

  public petersburgGallery: GalleryItem[] = [
    {title: `petersburg 1`, src: `assets/img/petersburg/image1.jpg`, description: `description`},
    {title: `petersburg 2`, src: `assets/img/petersburg/image2.jpg`, description: `description2`},
    {title: `petersburg 3`, src: `assets/img/petersburg/image3.jpg`, description: `description3`},
    {title: `petersburg 4`, src: `assets/img/petersburg/image4.jpg`, description: `description4`},
    {title: `petersburg 5`, src: `assets/img/petersburg/image5.jpg`, description: `description5`}
  ]

  public mainCards: MainItem[] = [
    {
      title: `Волгоград`, src: `assets/img/card1.jpg`, description: `Волгогра́д (до 1925 года — Цари́цын, до 1961 года — Сталингра́д) — город на юго-востоке европейской части России с 
    населением 1 004 763 человек (2021). Административный центр Волгоградской области. Город-герой, важнейший пункт обороны Царицына и Сталинградской битвы. Город областного 
    значения, образует городской округ.`, link: `volgograd`
    },
    {
      title: `Омск`,
      src: `assets/img/card2.jpg`,
      description: `Город-миллионер — 1 139 897 чел. (2021). Второй по численности населения в Сибири и девятый в России.`,
      link: `omsk`
    },
    {
      title: `Санкт-Петербург`,
      src: `assets/img/card3.jpg`,
      description: `Второй по численности населения город России. Город федерального значения. Административный центр 
    Северо-Западного федерального округа. Основан 16 (27) мая 1703 года царём Петром I. В 1714—1728 и 1732—1918 годах — столица Российского государства.`,
      link: `petersburg`
    },
  ]

  public mainCarousel: GalleryItem[] = [
    {
      title: `Волгоград`,
      src: `assets/img/sliders/slide1.jpg`,
      description: `Город на юго-востоке европейской части России.`
    },
    {
      title: `Омск`,
      src: `assets/img/sliders/slide2.jpg`,
      description: `Омск расположен на юге Западно-Сибирской равнины в южной подзоне лесостепной зоны на месте впадения в Иртыш реки Омь.`
    },
    {
      title: `Адлер`,
      src: `assets/img/sliders/slide3.jpg`,
      description: `Популярный летний российский курорт, а также место проведения форумов и крупных международных мероприятий.`
    },
  ]

  constructor() { }

}

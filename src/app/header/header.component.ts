import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
    selector: 'app-header',
    templateUrl: "header.component.html",
    styles: []
})
export class HeaderComponent implements OnInit {

    public _href: string = ``;

    constructor(private router: Router) {
    }

    ngOnInit() {
        this._href = this.router.url;
    }

    get href(): string {
        this._href = this.router.url;
        return this._href;
    }
}

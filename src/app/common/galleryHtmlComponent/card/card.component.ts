import { Component, Input } from '@angular/core';
import { GalleryItem, LoadingComponent } from "../../../interfaces/interfaces";

@Component({
    selector: 'app-card',
    template: `
			<div *ngIf="loading" class="spinner-container w-100 d-flex justify-content-center align-items-center">
				<div class="spinner-border" role="status">
					<span class="sr-only"></span>
				</div>
			</div>
			<img [hidden]="loading" src='{{item.src}}' (load)="onLoad()" class="card-img-top photo-album__image" alt={{item.title}} 
                 data-bs-target="#modal-carousel" attr.data-bs-slide-to="{{counter}}" attr.aria-label="Slide {{counter + 1}}">
			<div class="card-body">
				<p class="card-text">{{item.description}}</p>
			</div>
    `,
    styles: [
        `.spinner-container { min-height: 180px }`
    ]
})
export class CardComponent extends LoadingComponent {

    @Input() item: GalleryItem = {title: ``, src: ``, description: ``};
    @Input() counter: number = 0;

}

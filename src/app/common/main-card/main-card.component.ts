import { Component, Input } from '@angular/core';
import { LoadingComponent, MainItem } from "../../interfaces/interfaces";

@Component({
    selector: 'app-main-card',
    template: `
		<div *ngIf="loading" class="spinner-container w-100 d-flex justify-content-center align-items-center">
			<div class="spinner-border" role="status">
				<span class="sr-only"></span>
			</div>
		</div>
			<img src="{{item.src}}" class="card-img-top" alt="..." [hidden]="loading" (load)="onLoad()">
			<div class="card-body d-flex flex-column">
				<h5 class="card-title">{{item.title}}</h5>
				<p class="card-text">{{item.description}}</p>
				<a routerLink="{{item.link}}" class="btn btn-primary mt-auto">Перейти</a>
			</div>
    `,
    styles: [
        `.spinner-container { min-height: 200px }`
    ]
})
export class MainCardComponent extends LoadingComponent {

    @Input() item: MainItem = {title: ``, src: ``, description: ``, link: `/`};

}

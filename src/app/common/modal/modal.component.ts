import { Component, Input, OnInit } from '@angular/core';
import { GalleryItem } from "../../interfaces/interfaces";

@Component({
    selector: 'app-modal',
    templateUrl: 'modal.component.html',
    styles: []
})
export class ModalComponent implements OnInit {

    @Input() data: GalleryItem[] = [];
    @Input() title: string = ``;

    ngOnInit(): void {
    }


}

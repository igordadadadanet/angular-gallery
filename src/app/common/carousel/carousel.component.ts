import { Component, Input } from '@angular/core';
import { GalleryItem } from "../../interfaces/interfaces";

@Component({
    selector: 'app-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.css'],
})
export class CarouselComponent {

    @Input() showTitle = false;
    @Input() data: GalleryItem[] = [];

}

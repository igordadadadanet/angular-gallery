import { Component } from '@angular/core';
import { GalleryComponent, GalleryItem } from "../../interfaces/interfaces";
import { GalleryPageService } from "../../services/gallery-page.service";

@Component({
    selector: 'app-volgograd',
    templateUrl: '../../common/galleryHtmlComponent/galleryHtml.component.html',
    styles: [],
})
export class VolgogradComponent implements GalleryComponent {
    public data: GalleryItem[];
    title = "Волгоград";

    constructor(private galleryPageService: GalleryPageService) {
        this.data = this.galleryPageService.volgogradGallery
    }
}
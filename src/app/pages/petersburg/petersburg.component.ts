import { Component } from '@angular/core';
import { GalleryComponent, GalleryItem } from "../../interfaces/interfaces";
import { GalleryPageService } from "../../services/gallery-page.service";

@Component({
    selector: 'app-petersburg',
    templateUrl: '../../common/galleryHtmlComponent/galleryHtml.component.html',
    styles: [],
})
export class PetersburgComponent implements GalleryComponent {
    public data: GalleryItem[];
    title = "Санкт - Петербург";

    constructor(private galleryPageService: GalleryPageService) {
        this.data = this.galleryPageService.petersburgGallery
    }
}
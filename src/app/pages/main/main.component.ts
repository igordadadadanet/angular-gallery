import { Component } from '@angular/core';
import { GalleryItem, MainItem } from "../../interfaces/interfaces";
import { GalleryPageService } from "../../services/gallery-page.service";

@Component({
    selector: 'app-main',
    templateUrl: 'main.component.html',
    styles: []
})
export class MainComponent {
    public galleryData: GalleryItem[];

    public showTitle = true;

    public cardsData: MainItem[];

    constructor(private galleryPageService: GalleryPageService) {
        this.galleryData = this.galleryPageService.mainCarousel;
        this.cardsData = this.galleryPageService.mainCards;
    }
}
